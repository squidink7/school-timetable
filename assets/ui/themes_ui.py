# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'themes.ui'
##
## Created by: Qt User Interface Compiler version 6.2.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QDialog, QFrame, QHBoxLayout,
    QLabel, QLayout, QPushButton, QSizePolicy,
    QSpacerItem, QVBoxLayout, QWidget)
import screenshots_rc

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.setWindowModality(Qt.ApplicationModal)
        Dialog.resize(531, 475)
        Dialog.setModal(True)
        self.verticalLayout = QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.frameBack = QFrame(Dialog)
        self.frameBack.setObjectName(u"frameBack")
        self.frameBack.setStyleSheet(u"#frameBack {\n"
"border-radius: 10px;\n"
"}")
        self.frameBack.setFrameShape(QFrame.StyledPanel)
        self.frameBack.setFrameShadow(QFrame.Raised)
        self.verticalLayout_2 = QVBoxLayout(self.frameBack)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setSizeConstraint(QLayout.SetDefaultConstraint)
        self.frameDefault = QFrame(self.frameBack)
        self.frameDefault.setObjectName(u"frameDefault")
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.frameDefault.sizePolicy().hasHeightForWidth())
        self.frameDefault.setSizePolicy(sizePolicy)
        self.frameDefault.setCursor(QCursor(Qt.PointingHandCursor))
        self.frameDefault.setFrameShape(QFrame.StyledPanel)
        self.frameDefault.setFrameShadow(QFrame.Raised)
        self.verticalLayout_3 = QVBoxLayout(self.frameDefault)
        self.verticalLayout_3.setSpacing(0)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.imageDefault = QFrame(self.frameDefault)
        self.imageDefault.setObjectName(u"imageDefault")
        self.imageDefault.setStyleSheet(u"image: url(:/screenshots/default.png);")
        self.imageDefault.setFrameShape(QFrame.NoFrame)
        self.imageDefault.setFrameShadow(QFrame.Raised)

        self.verticalLayout_3.addWidget(self.imageDefault)

        self.horizontalLayout_5 = QHBoxLayout()
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.horizontalSpacer_2 = QSpacerItem(0, 0, QSizePolicy.MinimumExpanding, QSizePolicy.Minimum)

        self.horizontalLayout_5.addItem(self.horizontalSpacer_2)

        self.labelDefault = QLabel(self.frameDefault)
        self.labelDefault.setObjectName(u"labelDefault")
        sizePolicy1 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.labelDefault.sizePolicy().hasHeightForWidth())
        self.labelDefault.setSizePolicy(sizePolicy1)
        self.labelDefault.setAlignment(Qt.AlignCenter)
        self.labelDefault.setMargin(2)

        self.horizontalLayout_5.addWidget(self.labelDefault)

        self.horizontalSpacer_3 = QSpacerItem(0, 0, QSizePolicy.MinimumExpanding, QSizePolicy.Minimum)

        self.horizontalLayout_5.addItem(self.horizontalSpacer_3)


        self.verticalLayout_3.addLayout(self.horizontalLayout_5)


        self.horizontalLayout.addWidget(self.frameDefault)

        self.frameLight = QFrame(self.frameBack)
        self.frameLight.setObjectName(u"frameLight")
        sizePolicy.setHeightForWidth(self.frameLight.sizePolicy().hasHeightForWidth())
        self.frameLight.setSizePolicy(sizePolicy)
        self.frameLight.setCursor(QCursor(Qt.PointingHandCursor))
        self.frameLight.setFrameShape(QFrame.StyledPanel)
        self.frameLight.setFrameShadow(QFrame.Raised)
        self.verticalLayout_4 = QVBoxLayout(self.frameLight)
        self.verticalLayout_4.setSpacing(0)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.imageLight = QFrame(self.frameLight)
        self.imageLight.setObjectName(u"imageLight")
        self.imageLight.setStyleSheet(u"image: url(:/screenshots/light.png);")
        self.imageLight.setFrameShape(QFrame.NoFrame)
        self.imageLight.setFrameShadow(QFrame.Raised)

        self.verticalLayout_4.addWidget(self.imageLight)

        self.horizontalLayout_6 = QHBoxLayout()
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.horizontalSpacer_4 = QSpacerItem(0, 0, QSizePolicy.MinimumExpanding, QSizePolicy.Minimum)

        self.horizontalLayout_6.addItem(self.horizontalSpacer_4)

        self.labelLight = QLabel(self.frameLight)
        self.labelLight.setObjectName(u"labelLight")
        sizePolicy1.setHeightForWidth(self.labelLight.sizePolicy().hasHeightForWidth())
        self.labelLight.setSizePolicy(sizePolicy1)
        self.labelLight.setAlignment(Qt.AlignCenter)
        self.labelLight.setMargin(2)

        self.horizontalLayout_6.addWidget(self.labelLight)

        self.horizontalSpacer_5 = QSpacerItem(0, 0, QSizePolicy.MinimumExpanding, QSizePolicy.Minimum)

        self.horizontalLayout_6.addItem(self.horizontalSpacer_5)


        self.verticalLayout_4.addLayout(self.horizontalLayout_6)


        self.horizontalLayout.addWidget(self.frameLight)

        self.frameWoodcroft = QFrame(self.frameBack)
        self.frameWoodcroft.setObjectName(u"frameWoodcroft")
        sizePolicy.setHeightForWidth(self.frameWoodcroft.sizePolicy().hasHeightForWidth())
        self.frameWoodcroft.setSizePolicy(sizePolicy)
        self.frameWoodcroft.setCursor(QCursor(Qt.PointingHandCursor))
        self.frameWoodcroft.setFrameShape(QFrame.StyledPanel)
        self.frameWoodcroft.setFrameShadow(QFrame.Raised)
        self.verticalLayout_5 = QVBoxLayout(self.frameWoodcroft)
        self.verticalLayout_5.setSpacing(0)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.imageWoodcroft = QFrame(self.frameWoodcroft)
        self.imageWoodcroft.setObjectName(u"imageWoodcroft")
        self.imageWoodcroft.setStyleSheet(u"image: url(:/screenshots/woodcroft.png);")
        self.imageWoodcroft.setFrameShape(QFrame.NoFrame)
        self.imageWoodcroft.setFrameShadow(QFrame.Raised)

        self.verticalLayout_5.addWidget(self.imageWoodcroft)

        self.horizontalLayout_7 = QHBoxLayout()
        self.horizontalLayout_7.setObjectName(u"horizontalLayout_7")
        self.horizontalSpacer_6 = QSpacerItem(0, 0, QSizePolicy.MinimumExpanding, QSizePolicy.Minimum)

        self.horizontalLayout_7.addItem(self.horizontalSpacer_6)

        self.labelWoodcroft = QLabel(self.frameWoodcroft)
        self.labelWoodcroft.setObjectName(u"labelWoodcroft")
        sizePolicy1.setHeightForWidth(self.labelWoodcroft.sizePolicy().hasHeightForWidth())
        self.labelWoodcroft.setSizePolicy(sizePolicy1)
        self.labelWoodcroft.setAlignment(Qt.AlignCenter)
        self.labelWoodcroft.setMargin(2)

        self.horizontalLayout_7.addWidget(self.labelWoodcroft)

        self.horizontalSpacer_7 = QSpacerItem(0, 0, QSizePolicy.MinimumExpanding, QSizePolicy.Minimum)

        self.horizontalLayout_7.addItem(self.horizontalSpacer_7)


        self.verticalLayout_5.addLayout(self.horizontalLayout_7)


        self.horizontalLayout.addWidget(self.frameWoodcroft)

        self.frameHotDog = QFrame(self.frameBack)
        self.frameHotDog.setObjectName(u"frameHotDog")
        sizePolicy.setHeightForWidth(self.frameHotDog.sizePolicy().hasHeightForWidth())
        self.frameHotDog.setSizePolicy(sizePolicy)
        self.frameHotDog.setCursor(QCursor(Qt.PointingHandCursor))
        self.frameHotDog.setFrameShape(QFrame.StyledPanel)
        self.frameHotDog.setFrameShadow(QFrame.Raised)
        self.verticalLayout_6 = QVBoxLayout(self.frameHotDog)
        self.verticalLayout_6.setSpacing(0)
        self.verticalLayout_6.setObjectName(u"verticalLayout_6")
        self.imageHotDog = QFrame(self.frameHotDog)
        self.imageHotDog.setObjectName(u"imageHotDog")
        self.imageHotDog.setStyleSheet(u"image: url(:/screenshots/hotdog.png);")
        self.imageHotDog.setFrameShape(QFrame.NoFrame)
        self.imageHotDog.setFrameShadow(QFrame.Raised)

        self.verticalLayout_6.addWidget(self.imageHotDog)

        self.horizontalLayout_8 = QHBoxLayout()
        self.horizontalLayout_8.setObjectName(u"horizontalLayout_8")
        self.horizontalSpacer_8 = QSpacerItem(0, 0, QSizePolicy.MinimumExpanding, QSizePolicy.Minimum)

        self.horizontalLayout_8.addItem(self.horizontalSpacer_8)

        self.labelHotDog = QLabel(self.frameHotDog)
        self.labelHotDog.setObjectName(u"labelHotDog")
        sizePolicy1.setHeightForWidth(self.labelHotDog.sizePolicy().hasHeightForWidth())
        self.labelHotDog.setSizePolicy(sizePolicy1)
        self.labelHotDog.setAlignment(Qt.AlignCenter)
        self.labelHotDog.setMargin(2)

        self.horizontalLayout_8.addWidget(self.labelHotDog)

        self.horizontalSpacer_9 = QSpacerItem(0, 0, QSizePolicy.MinimumExpanding, QSizePolicy.Minimum)

        self.horizontalLayout_8.addItem(self.horizontalSpacer_9)


        self.verticalLayout_6.addLayout(self.horizontalLayout_8)


        self.horizontalLayout.addWidget(self.frameHotDog)


        self.verticalLayout_2.addLayout(self.horizontalLayout)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.frameGalaxy = QFrame(self.frameBack)
        self.frameGalaxy.setObjectName(u"frameGalaxy")
        sizePolicy.setHeightForWidth(self.frameGalaxy.sizePolicy().hasHeightForWidth())
        self.frameGalaxy.setSizePolicy(sizePolicy)
        self.frameGalaxy.setCursor(QCursor(Qt.PointingHandCursor))
        self.frameGalaxy.setFrameShape(QFrame.StyledPanel)
        self.frameGalaxy.setFrameShadow(QFrame.Raised)
        self.verticalLayout_7 = QVBoxLayout(self.frameGalaxy)
        self.verticalLayout_7.setSpacing(0)
        self.verticalLayout_7.setObjectName(u"verticalLayout_7")
        self.imageGalaxy = QFrame(self.frameGalaxy)
        self.imageGalaxy.setObjectName(u"imageGalaxy")
        self.imageGalaxy.setStyleSheet(u"image: url(:/screenshots/galaxy.png);")
        self.imageGalaxy.setFrameShape(QFrame.NoFrame)
        self.imageGalaxy.setFrameShadow(QFrame.Raised)

        self.verticalLayout_7.addWidget(self.imageGalaxy)

        self.horizontalLayout_9 = QHBoxLayout()
        self.horizontalLayout_9.setObjectName(u"horizontalLayout_9")
        self.horizontalSpacer_11 = QSpacerItem(0, 0, QSizePolicy.Preferred, QSizePolicy.Minimum)

        self.horizontalLayout_9.addItem(self.horizontalSpacer_11)

        self.labelGalaxy = QLabel(self.frameGalaxy)
        self.labelGalaxy.setObjectName(u"labelGalaxy")
        sizePolicy1.setHeightForWidth(self.labelGalaxy.sizePolicy().hasHeightForWidth())
        self.labelGalaxy.setSizePolicy(sizePolicy1)
        self.labelGalaxy.setAlignment(Qt.AlignCenter)
        self.labelGalaxy.setMargin(2)

        self.horizontalLayout_9.addWidget(self.labelGalaxy)

        self.horizontalSpacer_10 = QSpacerItem(0, 0, QSizePolicy.Preferred, QSizePolicy.Minimum)

        self.horizontalLayout_9.addItem(self.horizontalSpacer_10)


        self.verticalLayout_7.addLayout(self.horizontalLayout_9)


        self.horizontalLayout_2.addWidget(self.frameGalaxy)

        self.frameMeenu = QFrame(self.frameBack)
        self.frameMeenu.setObjectName(u"frameMeenu")
        sizePolicy.setHeightForWidth(self.frameMeenu.sizePolicy().hasHeightForWidth())
        self.frameMeenu.setSizePolicy(sizePolicy)
        self.frameMeenu.setCursor(QCursor(Qt.PointingHandCursor))
        self.frameMeenu.setFrameShape(QFrame.StyledPanel)
        self.frameMeenu.setFrameShadow(QFrame.Raised)
        self.verticalLayout_8 = QVBoxLayout(self.frameMeenu)
        self.verticalLayout_8.setSpacing(0)
        self.verticalLayout_8.setObjectName(u"verticalLayout_8")
        self.imageMeenu = QFrame(self.frameMeenu)
        self.imageMeenu.setObjectName(u"imageMeenu")
        self.imageMeenu.setStyleSheet(u"image: url(:/screenshots/meenu.png);")
        self.imageMeenu.setFrameShape(QFrame.NoFrame)
        self.imageMeenu.setFrameShadow(QFrame.Raised)

        self.verticalLayout_8.addWidget(self.imageMeenu)

        self.horizontalLayout_10 = QHBoxLayout()
        self.horizontalLayout_10.setObjectName(u"horizontalLayout_10")
        self.horizontalSpacer_12 = QSpacerItem(0, 0, QSizePolicy.Preferred, QSizePolicy.Minimum)

        self.horizontalLayout_10.addItem(self.horizontalSpacer_12)

        self.labelMeenu = QLabel(self.frameMeenu)
        self.labelMeenu.setObjectName(u"labelMeenu")
        sizePolicy1.setHeightForWidth(self.labelMeenu.sizePolicy().hasHeightForWidth())
        self.labelMeenu.setSizePolicy(sizePolicy1)
        self.labelMeenu.setAlignment(Qt.AlignCenter)
        self.labelMeenu.setMargin(2)

        self.horizontalLayout_10.addWidget(self.labelMeenu)

        self.horizontalSpacer_13 = QSpacerItem(0, 0, QSizePolicy.Preferred, QSizePolicy.Minimum)

        self.horizontalLayout_10.addItem(self.horizontalSpacer_13)


        self.verticalLayout_8.addLayout(self.horizontalLayout_10)


        self.horizontalLayout_2.addWidget(self.frameMeenu)

        self.frameKeely = QFrame(self.frameBack)
        self.frameKeely.setObjectName(u"frameKeely")
        sizePolicy.setHeightForWidth(self.frameKeely.sizePolicy().hasHeightForWidth())
        self.frameKeely.setSizePolicy(sizePolicy)
        self.frameKeely.setCursor(QCursor(Qt.PointingHandCursor))
        self.frameKeely.setFrameShape(QFrame.StyledPanel)
        self.frameKeely.setFrameShadow(QFrame.Raised)
        self.verticalLayout_9 = QVBoxLayout(self.frameKeely)
        self.verticalLayout_9.setSpacing(0)
        self.verticalLayout_9.setObjectName(u"verticalLayout_9")
        self.imageKeely = QFrame(self.frameKeely)
        self.imageKeely.setObjectName(u"imageKeely")
        self.imageKeely.setStyleSheet(u"image: url(:/screenshots/keely.png);")
        self.imageKeely.setFrameShape(QFrame.NoFrame)
        self.imageKeely.setFrameShadow(QFrame.Raised)

        self.verticalLayout_9.addWidget(self.imageKeely)

        self.horizontalLayout_11 = QHBoxLayout()
        self.horizontalLayout_11.setObjectName(u"horizontalLayout_11")
        self.horizontalSpacer_14 = QSpacerItem(0, 0, QSizePolicy.Preferred, QSizePolicy.Minimum)

        self.horizontalLayout_11.addItem(self.horizontalSpacer_14)

        self.labelKeely = QLabel(self.frameKeely)
        self.labelKeely.setObjectName(u"labelKeely")
        sizePolicy1.setHeightForWidth(self.labelKeely.sizePolicy().hasHeightForWidth())
        self.labelKeely.setSizePolicy(sizePolicy1)
        self.labelKeely.setAlignment(Qt.AlignCenter)
        self.labelKeely.setMargin(2)

        self.horizontalLayout_11.addWidget(self.labelKeely)

        self.horizontalSpacer_15 = QSpacerItem(0, 0, QSizePolicy.Preferred, QSizePolicy.Minimum)

        self.horizontalLayout_11.addItem(self.horizontalSpacer_15)


        self.verticalLayout_9.addLayout(self.horizontalLayout_11)


        self.horizontalLayout_2.addWidget(self.frameKeely)

        self.frameCommodore = QFrame(self.frameBack)
        self.frameCommodore.setObjectName(u"frameCommodore")
        sizePolicy.setHeightForWidth(self.frameCommodore.sizePolicy().hasHeightForWidth())
        self.frameCommodore.setSizePolicy(sizePolicy)
        self.frameCommodore.setCursor(QCursor(Qt.PointingHandCursor))
        self.frameCommodore.setFrameShape(QFrame.StyledPanel)
        self.frameCommodore.setFrameShadow(QFrame.Raised)
        self.verticalLayout_10 = QVBoxLayout(self.frameCommodore)
        self.verticalLayout_10.setSpacing(0)
        self.verticalLayout_10.setObjectName(u"verticalLayout_10")
        self.imageCommodore = QFrame(self.frameCommodore)
        self.imageCommodore.setObjectName(u"imageCommodore")
        self.imageCommodore.setStyleSheet(u"image: url(:/screenshots/commodore.png);")
        self.imageCommodore.setFrameShape(QFrame.NoFrame)
        self.imageCommodore.setFrameShadow(QFrame.Raised)

        self.verticalLayout_10.addWidget(self.imageCommodore)

        self.horizontalLayout_12 = QHBoxLayout()
        self.horizontalLayout_12.setObjectName(u"horizontalLayout_12")
        self.horizontalSpacer_16 = QSpacerItem(0, 0, QSizePolicy.Preferred, QSizePolicy.Minimum)

        self.horizontalLayout_12.addItem(self.horizontalSpacer_16)

        self.labelCommodore = QLabel(self.frameCommodore)
        self.labelCommodore.setObjectName(u"labelCommodore")
        sizePolicy1.setHeightForWidth(self.labelCommodore.sizePolicy().hasHeightForWidth())
        self.labelCommodore.setSizePolicy(sizePolicy1)
        self.labelCommodore.setAlignment(Qt.AlignCenter)
        self.labelCommodore.setMargin(2)

        self.horizontalLayout_12.addWidget(self.labelCommodore)

        self.horizontalSpacer_17 = QSpacerItem(0, 0, QSizePolicy.Preferred, QSizePolicy.Minimum)

        self.horizontalLayout_12.addItem(self.horizontalSpacer_17)


        self.verticalLayout_10.addLayout(self.horizontalLayout_12)


        self.horizontalLayout_2.addWidget(self.frameCommodore)


        self.verticalLayout_2.addLayout(self.horizontalLayout_2)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.frameTerminal = QFrame(self.frameBack)
        self.frameTerminal.setObjectName(u"frameTerminal")
        sizePolicy.setHeightForWidth(self.frameTerminal.sizePolicy().hasHeightForWidth())
        self.frameTerminal.setSizePolicy(sizePolicy)
        self.frameTerminal.setCursor(QCursor(Qt.PointingHandCursor))
        self.frameTerminal.setFrameShape(QFrame.StyledPanel)
        self.frameTerminal.setFrameShadow(QFrame.Raised)
        self.verticalLayout_11 = QVBoxLayout(self.frameTerminal)
        self.verticalLayout_11.setSpacing(0)
        self.verticalLayout_11.setObjectName(u"verticalLayout_11")
        self.imageTerminal = QFrame(self.frameTerminal)
        self.imageTerminal.setObjectName(u"imageTerminal")
        self.imageTerminal.setStyleSheet(u"image: url(:/screenshots/terminal.png);")
        self.imageTerminal.setFrameShape(QFrame.NoFrame)
        self.imageTerminal.setFrameShadow(QFrame.Raised)

        self.verticalLayout_11.addWidget(self.imageTerminal)

        self.horizontalLayout_13 = QHBoxLayout()
        self.horizontalLayout_13.setObjectName(u"horizontalLayout_13")
        self.horizontalSpacer_18 = QSpacerItem(0, 0, QSizePolicy.Preferred, QSizePolicy.Minimum)

        self.horizontalLayout_13.addItem(self.horizontalSpacer_18)

        self.labelTerminal = QLabel(self.frameTerminal)
        self.labelTerminal.setObjectName(u"labelTerminal")
        sizePolicy1.setHeightForWidth(self.labelTerminal.sizePolicy().hasHeightForWidth())
        self.labelTerminal.setSizePolicy(sizePolicy1)
        self.labelTerminal.setAlignment(Qt.AlignCenter)
        self.labelTerminal.setMargin(2)

        self.horizontalLayout_13.addWidget(self.labelTerminal)

        self.horizontalSpacer_19 = QSpacerItem(0, 0, QSizePolicy.Preferred, QSizePolicy.Minimum)

        self.horizontalLayout_13.addItem(self.horizontalSpacer_19)


        self.verticalLayout_11.addLayout(self.horizontalLayout_13)


        self.horizontalLayout_3.addWidget(self.frameTerminal)

        self.frameUbuntu = QFrame(self.frameBack)
        self.frameUbuntu.setObjectName(u"frameUbuntu")
        sizePolicy.setHeightForWidth(self.frameUbuntu.sizePolicy().hasHeightForWidth())
        self.frameUbuntu.setSizePolicy(sizePolicy)
        self.frameUbuntu.setCursor(QCursor(Qt.PointingHandCursor))
        self.frameUbuntu.setFrameShape(QFrame.StyledPanel)
        self.frameUbuntu.setFrameShadow(QFrame.Raised)
        self.verticalLayout_12 = QVBoxLayout(self.frameUbuntu)
        self.verticalLayout_12.setSpacing(0)
        self.verticalLayout_12.setObjectName(u"verticalLayout_12")
        self.imageUbuntu = QFrame(self.frameUbuntu)
        self.imageUbuntu.setObjectName(u"imageUbuntu")
        self.imageUbuntu.setStyleSheet(u"image: url(:/screenshots/ubuntu.png);")
        self.imageUbuntu.setFrameShape(QFrame.NoFrame)
        self.imageUbuntu.setFrameShadow(QFrame.Raised)

        self.verticalLayout_12.addWidget(self.imageUbuntu)

        self.horizontalLayout_14 = QHBoxLayout()
        self.horizontalLayout_14.setObjectName(u"horizontalLayout_14")
        self.horizontalSpacer_20 = QSpacerItem(0, 0, QSizePolicy.Preferred, QSizePolicy.Minimum)

        self.horizontalLayout_14.addItem(self.horizontalSpacer_20)

        self.labelUbuntu = QLabel(self.frameUbuntu)
        self.labelUbuntu.setObjectName(u"labelUbuntu")
        sizePolicy1.setHeightForWidth(self.labelUbuntu.sizePolicy().hasHeightForWidth())
        self.labelUbuntu.setSizePolicy(sizePolicy1)
        self.labelUbuntu.setAlignment(Qt.AlignCenter)
        self.labelUbuntu.setMargin(2)

        self.horizontalLayout_14.addWidget(self.labelUbuntu)

        self.horizontalSpacer_21 = QSpacerItem(0, 0, QSizePolicy.Preferred, QSizePolicy.Minimum)

        self.horizontalLayout_14.addItem(self.horizontalSpacer_21)


        self.verticalLayout_12.addLayout(self.horizontalLayout_14)


        self.horizontalLayout_3.addWidget(self.frameUbuntu)

        self.frameTheme0 = QFrame(self.frameBack)
        self.frameTheme0.setObjectName(u"frameTheme0")
        sizePolicy.setHeightForWidth(self.frameTheme0.sizePolicy().hasHeightForWidth())
        self.frameTheme0.setSizePolicy(sizePolicy)
        self.frameTheme0.setCursor(QCursor(Qt.PointingHandCursor))
        self.frameTheme0.setFrameShape(QFrame.StyledPanel)
        self.frameTheme0.setFrameShadow(QFrame.Raised)
        self.verticalLayout_13 = QVBoxLayout(self.frameTheme0)
        self.verticalLayout_13.setSpacing(0)
        self.verticalLayout_13.setObjectName(u"verticalLayout_13")
        self.imagaTheme0 = QFrame(self.frameTheme0)
        self.imagaTheme0.setObjectName(u"imagaTheme0")
        self.imagaTheme0.setStyleSheet(u"")
        self.imagaTheme0.setFrameShape(QFrame.NoFrame)
        self.imagaTheme0.setFrameShadow(QFrame.Raised)

        self.verticalLayout_13.addWidget(self.imagaTheme0)

        self.horizontalLayout_15 = QHBoxLayout()
        self.horizontalLayout_15.setObjectName(u"horizontalLayout_15")
        self.horizontalSpacer_22 = QSpacerItem(0, 0, QSizePolicy.Preferred, QSizePolicy.Minimum)

        self.horizontalLayout_15.addItem(self.horizontalSpacer_22)

        self.labelTheme0 = QLabel(self.frameTheme0)
        self.labelTheme0.setObjectName(u"labelTheme0")
        sizePolicy1.setHeightForWidth(self.labelTheme0.sizePolicy().hasHeightForWidth())
        self.labelTheme0.setSizePolicy(sizePolicy1)
        self.labelTheme0.setAlignment(Qt.AlignCenter)
        self.labelTheme0.setMargin(2)

        self.horizontalLayout_15.addWidget(self.labelTheme0)

        self.horizontalSpacer_23 = QSpacerItem(0, 0, QSizePolicy.Preferred, QSizePolicy.Minimum)

        self.horizontalLayout_15.addItem(self.horizontalSpacer_23)


        self.verticalLayout_13.addLayout(self.horizontalLayout_15)


        self.horizontalLayout_3.addWidget(self.frameTheme0)

        self.frameTheme3 = QFrame(self.frameBack)
        self.frameTheme3.setObjectName(u"frameTheme3")
        sizePolicy.setHeightForWidth(self.frameTheme3.sizePolicy().hasHeightForWidth())
        self.frameTheme3.setSizePolicy(sizePolicy)
        self.frameTheme3.setCursor(QCursor(Qt.PointingHandCursor))
        self.frameTheme3.setFrameShape(QFrame.StyledPanel)
        self.frameTheme3.setFrameShadow(QFrame.Raised)
        self.verticalLayout_18 = QVBoxLayout(self.frameTheme3)
        self.verticalLayout_18.setSpacing(0)
        self.verticalLayout_18.setObjectName(u"verticalLayout_18")
        self.imageTheme3 = QFrame(self.frameTheme3)
        self.imageTheme3.setObjectName(u"imageTheme3")
        self.imageTheme3.setStyleSheet(u"")
        self.imageTheme3.setFrameShape(QFrame.NoFrame)
        self.imageTheme3.setFrameShadow(QFrame.Raised)

        self.verticalLayout_18.addWidget(self.imageTheme3)

        self.horizontalLayout_21 = QHBoxLayout()
        self.horizontalLayout_21.setObjectName(u"horizontalLayout_21")
        self.horizontalSpacer_32 = QSpacerItem(0, 0, QSizePolicy.Preferred, QSizePolicy.Minimum)

        self.horizontalLayout_21.addItem(self.horizontalSpacer_32)

        self.labelTheme3 = QLabel(self.frameTheme3)
        self.labelTheme3.setObjectName(u"labelTheme3")
        sizePolicy1.setHeightForWidth(self.labelTheme3.sizePolicy().hasHeightForWidth())
        self.labelTheme3.setSizePolicy(sizePolicy1)
        self.labelTheme3.setAlignment(Qt.AlignCenter)
        self.labelTheme3.setMargin(2)

        self.horizontalLayout_21.addWidget(self.labelTheme3)

        self.horizontalSpacer_33 = QSpacerItem(0, 0, QSizePolicy.Preferred, QSizePolicy.Minimum)

        self.horizontalLayout_21.addItem(self.horizontalSpacer_33)


        self.verticalLayout_18.addLayout(self.horizontalLayout_21)


        self.horizontalLayout_3.addWidget(self.frameTheme3)


        self.verticalLayout_2.addLayout(self.horizontalLayout_3)

        self.horizontalLayout_17 = QHBoxLayout()
        self.horizontalLayout_17.setObjectName(u"horizontalLayout_17")
        self.frameBlueFlame = QFrame(self.frameBack)
        self.frameBlueFlame.setObjectName(u"frameBlueFlame")
        sizePolicy.setHeightForWidth(self.frameBlueFlame.sizePolicy().hasHeightForWidth())
        self.frameBlueFlame.setSizePolicy(sizePolicy)
        self.frameBlueFlame.setCursor(QCursor(Qt.PointingHandCursor))
        self.frameBlueFlame.setFrameShape(QFrame.StyledPanel)
        self.frameBlueFlame.setFrameShadow(QFrame.Raised)
        self.verticalLayout_15 = QVBoxLayout(self.frameBlueFlame)
        self.verticalLayout_15.setSpacing(0)
        self.verticalLayout_15.setObjectName(u"verticalLayout_15")
        self.imageBlueFlame = QFrame(self.frameBlueFlame)
        self.imageBlueFlame.setObjectName(u"imageBlueFlame")
        self.imageBlueFlame.setStyleSheet(u"image: url(:/screenshots/blueflame.png);")
        self.imageBlueFlame.setFrameShape(QFrame.NoFrame)
        self.imageBlueFlame.setFrameShadow(QFrame.Raised)

        self.verticalLayout_15.addWidget(self.imageBlueFlame)

        self.horizontalLayout_18 = QHBoxLayout()
        self.horizontalLayout_18.setObjectName(u"horizontalLayout_18")
        self.horizontalSpacer_26 = QSpacerItem(0, 0, QSizePolicy.Preferred, QSizePolicy.Minimum)

        self.horizontalLayout_18.addItem(self.horizontalSpacer_26)

        self.labelBlueFlame = QLabel(self.frameBlueFlame)
        self.labelBlueFlame.setObjectName(u"labelBlueFlame")
        sizePolicy1.setHeightForWidth(self.labelBlueFlame.sizePolicy().hasHeightForWidth())
        self.labelBlueFlame.setSizePolicy(sizePolicy1)
        self.labelBlueFlame.setAlignment(Qt.AlignCenter)
        self.labelBlueFlame.setMargin(2)

        self.horizontalLayout_18.addWidget(self.labelBlueFlame)

        self.horizontalSpacer_27 = QSpacerItem(0, 0, QSizePolicy.Preferred, QSizePolicy.Minimum)

        self.horizontalLayout_18.addItem(self.horizontalSpacer_27)


        self.verticalLayout_15.addLayout(self.horizontalLayout_18)


        self.horizontalLayout_17.addWidget(self.frameBlueFlame)

        self.frameTheme = QFrame(self.frameBack)
        self.frameTheme.setObjectName(u"frameTheme")
        sizePolicy.setHeightForWidth(self.frameTheme.sizePolicy().hasHeightForWidth())
        self.frameTheme.setSizePolicy(sizePolicy)
        self.frameTheme.setCursor(QCursor(Qt.PointingHandCursor))
        self.frameTheme.setFrameShape(QFrame.StyledPanel)
        self.frameTheme.setFrameShadow(QFrame.Raised)
        self.verticalLayout_16 = QVBoxLayout(self.frameTheme)
        self.verticalLayout_16.setSpacing(0)
        self.verticalLayout_16.setObjectName(u"verticalLayout_16")
        self.imageTheme = QFrame(self.frameTheme)
        self.imageTheme.setObjectName(u"imageTheme")
        self.imageTheme.setStyleSheet(u"")
        self.imageTheme.setFrameShape(QFrame.NoFrame)
        self.imageTheme.setFrameShadow(QFrame.Raised)

        self.verticalLayout_16.addWidget(self.imageTheme)

        self.horizontalLayout_19 = QHBoxLayout()
        self.horizontalLayout_19.setObjectName(u"horizontalLayout_19")
        self.horizontalSpacer_28 = QSpacerItem(0, 0, QSizePolicy.Preferred, QSizePolicy.Minimum)

        self.horizontalLayout_19.addItem(self.horizontalSpacer_28)

        self.labelTheme = QLabel(self.frameTheme)
        self.labelTheme.setObjectName(u"labelTheme")
        sizePolicy1.setHeightForWidth(self.labelTheme.sizePolicy().hasHeightForWidth())
        self.labelTheme.setSizePolicy(sizePolicy1)
        self.labelTheme.setAlignment(Qt.AlignCenter)
        self.labelTheme.setMargin(2)

        self.horizontalLayout_19.addWidget(self.labelTheme)

        self.horizontalSpacer_29 = QSpacerItem(0, 0, QSizePolicy.Preferred, QSizePolicy.Minimum)

        self.horizontalLayout_19.addItem(self.horizontalSpacer_29)


        self.verticalLayout_16.addLayout(self.horizontalLayout_19)


        self.horizontalLayout_17.addWidget(self.frameTheme)

        self.frameTheme2 = QFrame(self.frameBack)
        self.frameTheme2.setObjectName(u"frameTheme2")
        sizePolicy.setHeightForWidth(self.frameTheme2.sizePolicy().hasHeightForWidth())
        self.frameTheme2.setSizePolicy(sizePolicy)
        self.frameTheme2.setCursor(QCursor(Qt.PointingHandCursor))
        self.frameTheme2.setFrameShape(QFrame.StyledPanel)
        self.frameTheme2.setFrameShadow(QFrame.Raised)
        self.verticalLayout_17 = QVBoxLayout(self.frameTheme2)
        self.verticalLayout_17.setSpacing(0)
        self.verticalLayout_17.setObjectName(u"verticalLayout_17")
        self.imageTheme2 = QFrame(self.frameTheme2)
        self.imageTheme2.setObjectName(u"imageTheme2")
        self.imageTheme2.setStyleSheet(u"")
        self.imageTheme2.setFrameShape(QFrame.NoFrame)
        self.imageTheme2.setFrameShadow(QFrame.Raised)

        self.verticalLayout_17.addWidget(self.imageTheme2)

        self.horizontalLayout_20 = QHBoxLayout()
        self.horizontalLayout_20.setObjectName(u"horizontalLayout_20")
        self.horizontalSpacer_30 = QSpacerItem(0, 0, QSizePolicy.Preferred, QSizePolicy.Minimum)

        self.horizontalLayout_20.addItem(self.horizontalSpacer_30)

        self.labelTheme2 = QLabel(self.frameTheme2)
        self.labelTheme2.setObjectName(u"labelTheme2")
        sizePolicy1.setHeightForWidth(self.labelTheme2.sizePolicy().hasHeightForWidth())
        self.labelTheme2.setSizePolicy(sizePolicy1)
        self.labelTheme2.setAlignment(Qt.AlignCenter)
        self.labelTheme2.setMargin(2)

        self.horizontalLayout_20.addWidget(self.labelTheme2)

        self.horizontalSpacer_31 = QSpacerItem(0, 0, QSizePolicy.Preferred, QSizePolicy.Minimum)

        self.horizontalLayout_20.addItem(self.horizontalSpacer_31)


        self.verticalLayout_17.addLayout(self.horizontalLayout_20)


        self.horizontalLayout_17.addWidget(self.frameTheme2)

        self.frameCustom = QFrame(self.frameBack)
        self.frameCustom.setObjectName(u"frameCustom")
        sizePolicy.setHeightForWidth(self.frameCustom.sizePolicy().hasHeightForWidth())
        self.frameCustom.setSizePolicy(sizePolicy)
        self.frameCustom.setCursor(QCursor(Qt.PointingHandCursor))
        self.frameCustom.setFrameShape(QFrame.StyledPanel)
        self.frameCustom.setFrameShadow(QFrame.Raised)
        self.verticalLayout_14 = QVBoxLayout(self.frameCustom)
        self.verticalLayout_14.setSpacing(0)
        self.verticalLayout_14.setObjectName(u"verticalLayout_14")
        self.imageCustom = QFrame(self.frameCustom)
        self.imageCustom.setObjectName(u"imageCustom")
        self.imageCustom.setStyleSheet(u"image: url(:/screenshots/default.png);")
        self.imageCustom.setFrameShape(QFrame.NoFrame)
        self.imageCustom.setFrameShadow(QFrame.Raised)

        self.verticalLayout_14.addWidget(self.imageCustom)

        self.horizontalLayout_16 = QHBoxLayout()
        self.horizontalLayout_16.setObjectName(u"horizontalLayout_16")
        self.horizontalSpacer_24 = QSpacerItem(0, 0, QSizePolicy.Preferred, QSizePolicy.Minimum)

        self.horizontalLayout_16.addItem(self.horizontalSpacer_24)

        self.labelCustom = QLabel(self.frameCustom)
        self.labelCustom.setObjectName(u"labelCustom")
        sizePolicy1.setHeightForWidth(self.labelCustom.sizePolicy().hasHeightForWidth())
        self.labelCustom.setSizePolicy(sizePolicy1)
        self.labelCustom.setAlignment(Qt.AlignCenter)
        self.labelCustom.setMargin(2)

        self.horizontalLayout_16.addWidget(self.labelCustom)

        self.buttonCustom = QPushButton(self.frameCustom)
        self.buttonCustom.setObjectName(u"buttonCustom")
        sizePolicy2 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.buttonCustom.sizePolicy().hasHeightForWidth())
        self.buttonCustom.setSizePolicy(sizePolicy2)
        self.buttonCustom.setMaximumSize(QSize(20, 20))
        icon = QIcon()
        iconThemeName = u"edit"
        if QIcon.hasThemeIcon(iconThemeName):
            icon = QIcon.fromTheme(iconThemeName)
        else:
            icon.addFile(u".", QSize(), QIcon.Normal, QIcon.Off)
        
        self.buttonCustom.setIcon(icon)

        self.horizontalLayout_16.addWidget(self.buttonCustom)

        self.horizontalSpacer_25 = QSpacerItem(0, 0, QSizePolicy.Preferred, QSizePolicy.Minimum)

        self.horizontalLayout_16.addItem(self.horizontalSpacer_25)


        self.verticalLayout_14.addLayout(self.horizontalLayout_16)


        self.horizontalLayout_17.addWidget(self.frameCustom)


        self.verticalLayout_2.addLayout(self.horizontalLayout_17)

        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setSpacing(6)
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.horizontalSpacer = QSpacerItem(40, 0, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_4.addItem(self.horizontalSpacer)

        self.buttonOK = QPushButton(self.frameBack)
        self.buttonOK.setObjectName(u"buttonOK")

        self.horizontalLayout_4.addWidget(self.buttonOK)


        self.verticalLayout_2.addLayout(self.horizontalLayout_4)


        self.verticalLayout.addWidget(self.frameBack)


        self.retranslateUi(Dialog)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Dialog", None))
        self.labelDefault.setText(QCoreApplication.translate("Dialog", u"Default", None))
        self.labelLight.setText(QCoreApplication.translate("Dialog", u"Light", None))
        self.labelWoodcroft.setText(QCoreApplication.translate("Dialog", u"Woodcroft", None))
        self.labelHotDog.setText(QCoreApplication.translate("Dialog", u"Hot Dog Stand", None))
        self.labelGalaxy.setText(QCoreApplication.translate("Dialog", u"Galaxy", None))
        self.labelMeenu.setText(QCoreApplication.translate("Dialog", u"Meenu's theme", None))
        self.labelKeely.setText(QCoreApplication.translate("Dialog", u"Keely's theme", None))
        self.labelCommodore.setText(QCoreApplication.translate("Dialog", u"Commodore", None))
        self.labelTerminal.setText(QCoreApplication.translate("Dialog", u"Terminal", None))
        self.labelUbuntu.setText(QCoreApplication.translate("Dialog", u"Ubuntu", None))
        self.labelTheme0.setText(QCoreApplication.translate("Dialog", u"Theme", None))
        self.labelTheme3.setText(QCoreApplication.translate("Dialog", u"Theme", None))
        self.labelBlueFlame.setText(QCoreApplication.translate("Dialog", u"Blue Flame", None))
        self.labelTheme.setText(QCoreApplication.translate("Dialog", u"Theme", None))
        self.labelTheme2.setText(QCoreApplication.translate("Dialog", u"Theme", None))
        self.labelCustom.setText(QCoreApplication.translate("Dialog", u"Custom", None))
        self.buttonCustom.setText("")
        self.buttonOK.setText(QCoreApplication.translate("Dialog", u"Ok", None))
    # retranslateUi

