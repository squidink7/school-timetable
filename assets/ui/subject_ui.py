# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'subject.ui'
##
## Created by: Qt User Interface Compiler version 6.2.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QCheckBox, QComboBox, QDialog,
    QFrame, QHBoxLayout, QLabel, QLineEdit,
    QPlainTextEdit, QPushButton, QSizePolicy, QSpacerItem,
    QVBoxLayout, QWidget)
import backgrounds_rc

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.setWindowModality(Qt.ApplicationModal)
        Dialog.resize(806, 414)
        Dialog.setMinimumSize(QSize(335, 400))
        Dialog.setMaximumSize(QSize(999, 440))
        Dialog.setModal(True)
        self.verticalLayout = QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.frameView = QFrame(Dialog)
        self.frameView.setObjectName(u"frameView")
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.frameView.sizePolicy().hasHeightForWidth())
        self.frameView.setSizePolicy(sizePolicy)
        self.frameView.setMinimumSize(QSize(378, 0))
        self.frameView.setAutoFillBackground(False)
        self.frameView.setStyleSheet(u"#frameView {\n"
"	border-image: url(:/Images/view.png);\n"
"	background-position: left bottom ;\n"
"	background-color: rgb(42, 42, 42);\n"
"	border-radius: 10px;\n"
"}")
        self.frameView.setFrameShape(QFrame.StyledPanel)
        self.frameView.setFrameShadow(QFrame.Raised)
        self.verticalLayout_3 = QVBoxLayout(self.frameView)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(7, -1, 7, -1)
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer)

        self.labelTitle = QLabel(self.frameView)
        self.labelTitle.setObjectName(u"labelTitle")
        sizePolicy1 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.labelTitle.sizePolicy().hasHeightForWidth())
        self.labelTitle.setSizePolicy(sizePolicy1)
        self.labelTitle.setMinimumSize(QSize(0, 40))
        self.labelTitle.setMaximumSize(QSize(16777215, 16777215))
        self.labelTitle.setStyleSheet(u"font: 75 20pt;\n"
"border-image: None;")
        self.labelTitle.setScaledContents(True)
        self.labelTitle.setAlignment(Qt.AlignCenter)
        self.labelTitle.setMargin(3)

        self.horizontalLayout_2.addWidget(self.labelTitle)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer_2)


        self.verticalLayout_3.addLayout(self.horizontalLayout_2)

        self.verticalSpacer_6 = QSpacerItem(20, 20, QSizePolicy.Minimum, QSizePolicy.Fixed)

        self.verticalLayout_3.addItem(self.verticalSpacer_6)

        self.horizontalLayout_5 = QHBoxLayout()
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.horizontalSpacer_14 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_5.addItem(self.horizontalSpacer_14)

        self.verticalLayout_labels = QVBoxLayout()
        self.verticalLayout_labels.setSpacing(8)
        self.verticalLayout_labels.setObjectName(u"verticalLayout_labels")
        self.displayTeacher = QHBoxLayout()
        self.displayTeacher.setSpacing(0)
        self.displayTeacher.setObjectName(u"displayTeacher")
        self.horizontalSpacer_3 = QSpacerItem(0, 0, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.displayTeacher.addItem(self.horizontalSpacer_3)

        self.labelTeacher = QLabel(self.frameView)
        self.labelTeacher.setObjectName(u"labelTeacher")
        self.labelTeacher.setMinimumSize(QSize(0, 21))
        self.labelTeacher.setStyleSheet(u"border-image: None;")
        self.labelTeacher.setAlignment(Qt.AlignRight|Qt.AlignTop|Qt.AlignTrailing)
        self.labelTeacher.setMargin(2)

        self.displayTeacher.addWidget(self.labelTeacher)


        self.verticalLayout_labels.addLayout(self.displayTeacher)

        self.displayRoom = QHBoxLayout()
        self.displayRoom.setSpacing(0)
        self.displayRoom.setObjectName(u"displayRoom")
        self.horizontalSpacer_4 = QSpacerItem(0, 0, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.displayRoom.addItem(self.horizontalSpacer_4)

        self.labelRoom = QLabel(self.frameView)
        self.labelRoom.setObjectName(u"labelRoom")
        self.labelRoom.setMinimumSize(QSize(0, 21))
        self.labelRoom.setStyleSheet(u"border-image: None;")
        self.labelRoom.setAlignment(Qt.AlignRight|Qt.AlignTop|Qt.AlignTrailing)
        self.labelRoom.setMargin(2)

        self.displayRoom.addWidget(self.labelRoom)


        self.verticalLayout_labels.addLayout(self.displayRoom)

        self.displayCanvas = QHBoxLayout()
        self.displayCanvas.setSpacing(0)
        self.displayCanvas.setObjectName(u"displayCanvas")
        self.horizontalSpacer_5 = QSpacerItem(0, 0, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.displayCanvas.addItem(self.horizontalSpacer_5)

        self.labelOnline = QLabel(self.frameView)
        self.labelOnline.setObjectName(u"labelOnline")
        self.labelOnline.setMinimumSize(QSize(0, 21))
        self.labelOnline.setStyleSheet(u"border-image: None;")
        self.labelOnline.setAlignment(Qt.AlignRight|Qt.AlignTop|Qt.AlignTrailing)
        self.labelOnline.setMargin(3)

        self.displayCanvas.addWidget(self.labelOnline)


        self.verticalLayout_labels.addLayout(self.displayCanvas)

        self.displayNotes = QHBoxLayout()
        self.displayNotes.setSpacing(0)
        self.displayNotes.setObjectName(u"displayNotes")
        self.horizontalSpacer_6 = QSpacerItem(0, 0, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.displayNotes.addItem(self.horizontalSpacer_6)

        self.labelNotes = QLabel(self.frameView)
        self.labelNotes.setObjectName(u"labelNotes")
        self.labelNotes.setMinimumSize(QSize(0, 21))
        self.labelNotes.setStyleSheet(u"border-image: None;")
        self.labelNotes.setAlignment(Qt.AlignRight|Qt.AlignTop|Qt.AlignTrailing)
        self.labelNotes.setMargin(2)

        self.displayNotes.addWidget(self.labelNotes)


        self.verticalLayout_labels.addLayout(self.displayNotes)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_labels.addItem(self.verticalSpacer)


        self.horizontalLayout_5.addLayout(self.verticalLayout_labels)

        self.verticalLayout_2 = QVBoxLayout()
        self.verticalLayout_2.setSpacing(8)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.teacher = QLabel(self.frameView)
        self.teacher.setObjectName(u"teacher")
        sizePolicy2 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.teacher.sizePolicy().hasHeightForWidth())
        self.teacher.setSizePolicy(sizePolicy2)
        self.teacher.setMinimumSize(QSize(0, 20))
        self.teacher.setStyleSheet(u"border-image: None;")
        self.teacher.setMargin(2)

        self.verticalLayout_2.addWidget(self.teacher)

        self.room = QPushButton(self.frameView)
        self.room.setObjectName(u"room")
        sizePolicy3 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.room.sizePolicy().hasHeightForWidth())
        self.room.setSizePolicy(sizePolicy3)
        self.room.setMaximumSize(QSize(16777215, 22))
        self.room.setFlat(False)

        self.verticalLayout_2.addWidget(self.room)

        self.online = QPushButton(self.frameView)
        self.online.setObjectName(u"online")
        sizePolicy3.setHeightForWidth(self.online.sizePolicy().hasHeightForWidth())
        self.online.setSizePolicy(sizePolicy3)
        self.online.setMaximumSize(QSize(16777215, 22))
        self.online.setFlat(False)

        self.verticalLayout_2.addWidget(self.online)

        self.notes = QLabel(self.frameView)
        self.notes.setObjectName(u"notes")
        sizePolicy4 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        sizePolicy4.setHorizontalStretch(0)
        sizePolicy4.setVerticalStretch(0)
        sizePolicy4.setHeightForWidth(self.notes.sizePolicy().hasHeightForWidth())
        self.notes.setSizePolicy(sizePolicy4)
        self.notes.setStyleSheet(u"")
        self.notes.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignTop)
        self.notes.setMargin(2)
        self.notes.setIndent(3)

        self.verticalLayout_2.addWidget(self.notes)

        self.verticalSpacer_2 = QSpacerItem(0, 0, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_2.addItem(self.verticalSpacer_2)


        self.horizontalLayout_5.addLayout(self.verticalLayout_2)

        self.horizontalSpacer_13 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_5.addItem(self.horizontalSpacer_13)


        self.verticalLayout_3.addLayout(self.horizontalLayout_5)

        self.verticalSpacer_4 = QSpacerItem(20, 93, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_3.addItem(self.verticalSpacer_4)

        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.buttonAssignment = QPushButton(self.frameView)
        self.buttonAssignment.setObjectName(u"buttonAssignment")
        sizePolicy5 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        sizePolicy5.setHorizontalStretch(0)
        sizePolicy5.setVerticalStretch(0)
        sizePolicy5.setHeightForWidth(self.buttonAssignment.sizePolicy().hasHeightForWidth())
        self.buttonAssignment.setSizePolicy(sizePolicy5)
        self.buttonAssignment.setFlat(False)

        self.horizontalLayout_4.addWidget(self.buttonAssignment)

        self.horizontalSpacer_15 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_4.addItem(self.horizontalSpacer_15)

        self.buttonBack = QPushButton(self.frameView)
        self.buttonBack.setObjectName(u"buttonBack")
        sizePolicy5.setHeightForWidth(self.buttonBack.sizePolicy().hasHeightForWidth())
        self.buttonBack.setSizePolicy(sizePolicy5)
        self.buttonBack.setFlat(False)

        self.horizontalLayout_4.addWidget(self.buttonBack)

        self.buttonEdit = QPushButton(self.frameView)
        self.buttonEdit.setObjectName(u"buttonEdit")
        sizePolicy5.setHeightForWidth(self.buttonEdit.sizePolicy().hasHeightForWidth())
        self.buttonEdit.setSizePolicy(sizePolicy5)
        self.buttonEdit.setFlat(False)

        self.horizontalLayout_4.addWidget(self.buttonEdit)


        self.verticalLayout_3.addLayout(self.horizontalLayout_4)


        self.horizontalLayout.addWidget(self.frameView)

        self.frameEdit = QFrame(Dialog)
        self.frameEdit.setObjectName(u"frameEdit")
        sizePolicy6 = QSizePolicy(QSizePolicy.Ignored, QSizePolicy.Preferred)
        sizePolicy6.setHorizontalStretch(0)
        sizePolicy6.setVerticalStretch(0)
        sizePolicy6.setHeightForWidth(self.frameEdit.sizePolicy().hasHeightForWidth())
        self.frameEdit.setSizePolicy(sizePolicy6)
        self.frameEdit.setMinimumSize(QSize(378, 0))
        self.frameEdit.setAutoFillBackground(False)
        self.frameEdit.setStyleSheet(u"QFrame {\n"
"	background-image: url(:/backgrounds/edit.png);\n"
"	background-position: left top;\n"
"	background-color: rgb(42, 42, 42);\n"
"	border-radius: 10px;\n"
"}")
        self.frameEdit.setFrameShape(QFrame.StyledPanel)
        self.frameEdit.setFrameShadow(QFrame.Raised)
        self.verticalLayout_4 = QVBoxLayout(self.frameEdit)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalLayout_3.setContentsMargins(7, -1, 7, -1)
        self.horizontalSpacer_7 = QSpacerItem(0, 0, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer_7)

        self.editTitle = QLineEdit(self.frameEdit)
        self.editTitle.setObjectName(u"editTitle")
        sizePolicy3.setHeightForWidth(self.editTitle.sizePolicy().hasHeightForWidth())
        self.editTitle.setSizePolicy(sizePolicy3)
        self.editTitle.setMinimumSize(QSize(0, 40))
        self.editTitle.setMaximumSize(QSize(150, 40))
        self.editTitle.setStyleSheet(u"font: 75 20pt;\n"
"border-radius: 5px;")
        self.editTitle.setMaxLength(13)
        self.editTitle.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_3.addWidget(self.editTitle)

        self.editColor = QComboBox(self.frameEdit)
        self.editColor.setObjectName(u"editColor")
        sizePolicy1.setHeightForWidth(self.editColor.sizePolicy().hasHeightForWidth())
        self.editColor.setSizePolicy(sizePolicy1)

        self.horizontalLayout_3.addWidget(self.editColor)

        self.horizontalSpacer_8 = QSpacerItem(0, 0, QSizePolicy.Preferred, QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer_8)


        self.verticalLayout_4.addLayout(self.horizontalLayout_3)

        self.verticalSpacer_3 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_4.addItem(self.verticalSpacer_3)

        self.horizontalLayout_7 = QHBoxLayout()
        self.horizontalLayout_7.setObjectName(u"horizontalLayout_7")
        self.horizontalSpacer_17 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_7.addItem(self.horizontalSpacer_17)

        self.verticalLayout_editLabels = QVBoxLayout()
        self.verticalLayout_editLabels.setSpacing(8)
        self.verticalLayout_editLabels.setObjectName(u"verticalLayout_editLabels")
        self.displayTeacher_3 = QHBoxLayout()
        self.displayTeacher_3.setSpacing(0)
        self.displayTeacher_3.setObjectName(u"displayTeacher_3")
        self.horizontalSpacer_21 = QSpacerItem(0, 0, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.displayTeacher_3.addItem(self.horizontalSpacer_21)

        self.labelEditTeacher = QLabel(self.frameEdit)
        self.labelEditTeacher.setObjectName(u"labelEditTeacher")
        self.labelEditTeacher.setMinimumSize(QSize(0, 21))
        self.labelEditTeacher.setStyleSheet(u"border-image: None;")
        self.labelEditTeacher.setAlignment(Qt.AlignRight|Qt.AlignTop|Qt.AlignTrailing)
        self.labelEditTeacher.setMargin(2)

        self.displayTeacher_3.addWidget(self.labelEditTeacher)


        self.verticalLayout_editLabels.addLayout(self.displayTeacher_3)

        self.displayRoom_3 = QHBoxLayout()
        self.displayRoom_3.setSpacing(0)
        self.displayRoom_3.setObjectName(u"displayRoom_3")
        self.horizontalSpacer_22 = QSpacerItem(0, 0, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.displayRoom_3.addItem(self.horizontalSpacer_22)

        self.labelEditRoom = QLabel(self.frameEdit)
        self.labelEditRoom.setObjectName(u"labelEditRoom")
        self.labelEditRoom.setMinimumSize(QSize(0, 21))
        self.labelEditRoom.setStyleSheet(u"border-image: None;")
        self.labelEditRoom.setAlignment(Qt.AlignRight|Qt.AlignTop|Qt.AlignTrailing)
        self.labelEditRoom.setMargin(2)

        self.displayRoom_3.addWidget(self.labelEditRoom)


        self.verticalLayout_editLabels.addLayout(self.displayRoom_3)

        self.displayCanvas_3 = QHBoxLayout()
        self.displayCanvas_3.setSpacing(0)
        self.displayCanvas_3.setObjectName(u"displayCanvas_3")
        self.horizontalSpacer_23 = QSpacerItem(0, 0, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.displayCanvas_3.addItem(self.horizontalSpacer_23)

        self.labelEditOnline = QLabel(self.frameEdit)
        self.labelEditOnline.setObjectName(u"labelEditOnline")
        self.labelEditOnline.setMinimumSize(QSize(0, 21))
        self.labelEditOnline.setStyleSheet(u"border-image: None;")
        self.labelEditOnline.setAlignment(Qt.AlignRight|Qt.AlignTop|Qt.AlignTrailing)
        self.labelEditOnline.setMargin(3)

        self.displayCanvas_3.addWidget(self.labelEditOnline)


        self.verticalLayout_editLabels.addLayout(self.displayCanvas_3)

        self.displayNotes_3 = QHBoxLayout()
        self.displayNotes_3.setSpacing(0)
        self.displayNotes_3.setObjectName(u"displayNotes_3")
        self.horizontalSpacer_24 = QSpacerItem(0, 0, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.displayNotes_3.addItem(self.horizontalSpacer_24)

        self.labelEditNotes = QLabel(self.frameEdit)
        self.labelEditNotes.setObjectName(u"labelEditNotes")
        self.labelEditNotes.setMinimumSize(QSize(0, 21))
        self.labelEditNotes.setStyleSheet(u"border-image: None;")
        self.labelEditNotes.setAlignment(Qt.AlignRight|Qt.AlignTop|Qt.AlignTrailing)
        self.labelEditNotes.setMargin(2)

        self.displayNotes_3.addWidget(self.labelEditNotes)


        self.verticalLayout_editLabels.addLayout(self.displayNotes_3)

        self.verticalSpacer_8 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_editLabels.addItem(self.verticalSpacer_8)


        self.horizontalLayout_7.addLayout(self.verticalLayout_editLabels)

        self.verticalLayout_7 = QVBoxLayout()
        self.verticalLayout_7.setSpacing(8)
        self.verticalLayout_7.setObjectName(u"verticalLayout_7")
        self.editTeacher = QLineEdit(self.frameEdit)
        self.editTeacher.setObjectName(u"editTeacher")
        sizePolicy2.setHeightForWidth(self.editTeacher.sizePolicy().hasHeightForWidth())
        self.editTeacher.setSizePolicy(sizePolicy2)
        self.editTeacher.setStyleSheet(u"border-radius: 3px;")
        self.editTeacher.setMaxLength(15)

        self.verticalLayout_7.addWidget(self.editTeacher)

        self.editRoom = QLineEdit(self.frameEdit)
        self.editRoom.setObjectName(u"editRoom")
        sizePolicy2.setHeightForWidth(self.editRoom.sizePolicy().hasHeightForWidth())
        self.editRoom.setSizePolicy(sizePolicy2)
        self.editRoom.setStyleSheet(u"border-radius: 3px;")
        self.editRoom.setMaxLength(7)

        self.verticalLayout_7.addWidget(self.editRoom)

        self.editOnline = QLineEdit(self.frameEdit)
        self.editOnline.setObjectName(u"editOnline")
        sizePolicy2.setHeightForWidth(self.editOnline.sizePolicy().hasHeightForWidth())
        self.editOnline.setSizePolicy(sizePolicy2)
        self.editOnline.setStyleSheet(u"border-radius: 3px;")

        self.verticalLayout_7.addWidget(self.editOnline)

        self.editNotes = QPlainTextEdit(self.frameEdit)
        self.editNotes.setObjectName(u"editNotes")
        sizePolicy7 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Expanding)
        sizePolicy7.setHorizontalStretch(0)
        sizePolicy7.setVerticalStretch(0)
        sizePolicy7.setHeightForWidth(self.editNotes.sizePolicy().hasHeightForWidth())
        self.editNotes.setSizePolicy(sizePolicy7)
        self.editNotes.setMaximumSize(QSize(90, 50))
        self.editNotes.setStyleSheet(u"border-radius: 3px;")
        self.editNotes.setFrameShape(QFrame.NoFrame)
        self.editNotes.setTabChangesFocus(True)

        self.verticalLayout_7.addWidget(self.editNotes)


        self.horizontalLayout_7.addLayout(self.verticalLayout_7)

        self.horizontalSpacer_18 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_7.addItem(self.horizontalSpacer_18)


        self.verticalLayout_4.addLayout(self.horizontalLayout_7)

        self.horizontalLayout_8 = QHBoxLayout()
        self.horizontalLayout_8.setObjectName(u"horizontalLayout_8")
        self.horizontalLayout_8.setContentsMargins(15, -1, 15, -1)
        self.labelDouble = QLabel(self.frameEdit)
        self.labelDouble.setObjectName(u"labelDouble")

        self.horizontalLayout_8.addWidget(self.labelDouble)

        self.comboDouble = QComboBox(self.frameEdit)
        self.comboDouble.setObjectName(u"comboDouble")

        self.horizontalLayout_8.addWidget(self.comboDouble)

        self.horizontalSpacer_19 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_8.addItem(self.horizontalSpacer_19)

        self.applyToAll = QCheckBox(self.frameEdit)
        self.applyToAll.setObjectName(u"applyToAll")

        self.horizontalLayout_8.addWidget(self.applyToAll)


        self.verticalLayout_4.addLayout(self.horizontalLayout_8)

        self.verticalSpacer_5 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_4.addItem(self.verticalSpacer_5)

        self.horizontalLayout_9 = QHBoxLayout()
        self.horizontalLayout_9.setObjectName(u"horizontalLayout_9")
        self.labelCopied = QLabel(self.frameEdit)
        self.labelCopied.setObjectName(u"labelCopied")
        sizePolicy1.setHeightForWidth(self.labelCopied.sizePolicy().hasHeightForWidth())
        self.labelCopied.setSizePolicy(sizePolicy1)
        self.labelCopied.setStyleSheet(u"")
        self.labelCopied.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)

        self.horizontalLayout_9.addWidget(self.labelCopied)

        self.horizontalSpacer_20 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_9.addItem(self.horizontalSpacer_20)


        self.verticalLayout_4.addLayout(self.horizontalLayout_9)

        self.horizontalLayout_6 = QHBoxLayout()
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.buttonCopy = QPushButton(self.frameEdit)
        self.buttonCopy.setObjectName(u"buttonCopy")
        icon = QIcon()
        iconThemeName = u"edit-copy"
        if QIcon.hasThemeIcon(iconThemeName):
            icon = QIcon.fromTheme(iconThemeName)
        else:
            icon.addFile(u".", QSize(), QIcon.Normal, QIcon.Off)
        
        self.buttonCopy.setIcon(icon)
        self.buttonCopy.setFlat(True)

        self.horizontalLayout_6.addWidget(self.buttonCopy)

        self.buttonPaste = QPushButton(self.frameEdit)
        self.buttonPaste.setObjectName(u"buttonPaste")
        icon1 = QIcon()
        iconThemeName = u"edit-paste"
        if QIcon.hasThemeIcon(iconThemeName):
            icon1 = QIcon.fromTheme(iconThemeName)
        else:
            icon1.addFile(u".", QSize(), QIcon.Normal, QIcon.Off)
        
        self.buttonPaste.setIcon(icon1)
        self.buttonPaste.setFlat(True)

        self.horizontalLayout_6.addWidget(self.buttonPaste)

        self.horizontalSpacer_16 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_6.addItem(self.horizontalSpacer_16)

        self.editButtonBack = QPushButton(self.frameEdit)
        self.editButtonBack.setObjectName(u"editButtonBack")
        sizePolicy5.setHeightForWidth(self.editButtonBack.sizePolicy().hasHeightForWidth())
        self.editButtonBack.setSizePolicy(sizePolicy5)
        self.editButtonBack.setFlat(False)

        self.horizontalLayout_6.addWidget(self.editButtonBack)

        self.editButtonSave = QPushButton(self.frameEdit)
        self.editButtonSave.setObjectName(u"editButtonSave")
        sizePolicy5.setHeightForWidth(self.editButtonSave.sizePolicy().hasHeightForWidth())
        self.editButtonSave.setSizePolicy(sizePolicy5)
        self.editButtonSave.setFlat(False)

        self.horizontalLayout_6.addWidget(self.editButtonSave)


        self.verticalLayout_4.addLayout(self.horizontalLayout_6)


        self.horizontalLayout.addWidget(self.frameEdit)


        self.verticalLayout.addLayout(self.horizontalLayout)


        self.retranslateUi(Dialog)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Dialog", None))
        self.labelTitle.setText(QCoreApplication.translate("Dialog", u"<html><head/><body><p><span style=\" font-weight:600;\">Subject</span></p></body></html>", None))
        self.labelTeacher.setText(QCoreApplication.translate("Dialog", u"Teacher:", None))
        self.labelRoom.setText(QCoreApplication.translate("Dialog", u"Room:", None))
        self.labelOnline.setText(QCoreApplication.translate("Dialog", u"Online:", None))
        self.labelNotes.setText(QCoreApplication.translate("Dialog", u"Notes:", None))
        self.teacher.setText(QCoreApplication.translate("Dialog", u"[]", None))
        self.room.setText(QCoreApplication.translate("Dialog", u"[]", None))
        self.online.setText(QCoreApplication.translate("Dialog", u"Open Link", None))
        self.notes.setText(QCoreApplication.translate("Dialog", u"<html><head/><body><p>[]</p></body></html>", None))
        self.buttonAssignment.setText(QCoreApplication.translate("Dialog", u"Assignments", None))
        self.buttonBack.setText(QCoreApplication.translate("Dialog", u"Close", None))
        self.buttonEdit.setText(QCoreApplication.translate("Dialog", u"Edit", None))
        self.editTitle.setPlaceholderText(QCoreApplication.translate("Dialog", u"Name", None))
        self.labelEditTeacher.setText(QCoreApplication.translate("Dialog", u"Teacher:", None))
        self.labelEditRoom.setText(QCoreApplication.translate("Dialog", u"Room:", None))
        self.labelEditOnline.setText(QCoreApplication.translate("Dialog", u"Online:", None))
        self.labelEditNotes.setText(QCoreApplication.translate("Dialog", u"Notes:", None))
        self.editTeacher.setPlaceholderText(QCoreApplication.translate("Dialog", u"Name", None))
        self.editRoom.setPlaceholderText(QCoreApplication.translate("Dialog", u"Number", None))
        self.editOnline.setPlaceholderText(QCoreApplication.translate("Dialog", u"URL", None))
        self.editNotes.setPlainText("")
        self.labelDouble.setText(QCoreApplication.translate("Dialog", u"Double:", None))
        self.applyToAll.setText(QCoreApplication.translate("Dialog", u"Apply to all", None))
        self.labelCopied.setText(QCoreApplication.translate("Dialog", u"Copied", None))
        self.buttonCopy.setText("")
        self.buttonPaste.setText("")
        self.editButtonBack.setText(QCoreApplication.translate("Dialog", u"Back", None))
        self.editButtonSave.setText(QCoreApplication.translate("Dialog", u"Save", None))
    # retranslateUi

