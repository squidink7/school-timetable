# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'settings.ui'
##
## Created by: Qt User Interface Compiler version 6.2.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QCheckBox, QDialog, QFrame,
    QHBoxLayout, QLabel, QLineEdit, QPushButton,
    QSizePolicy, QSpacerItem, QVBoxLayout, QWidget)
import backgrounds_rc

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.setWindowModality(Qt.ApplicationModal)
        Dialog.resize(460, 335)
        Dialog.setModal(True)
        self.verticalLayout_3 = QVBoxLayout(Dialog)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.verticalLayout_2 = QVBoxLayout()
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.frame = QFrame(Dialog)
        self.frame.setObjectName(u"frame")
        self.frame.setStyleSheet(u"QFrame {\n"
"	border-image: url(:/Images/settings.png);\n"
"	background-position: top;\n"
"	background-color: rgb(42, 42, 42);\n"
"	border-radius: 10px;\n"
"}")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.verticalLayout = QVBoxLayout(self.frame)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalSpacer_5 = QSpacerItem(20, 14, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer_5)

        self.horizontalLayout_5 = QHBoxLayout()
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_5.addItem(self.horizontalSpacer_2)

        self.labelTitle = QLabel(self.frame)
        self.labelTitle.setObjectName(u"labelTitle")
        self.labelTitle.setStyleSheet(u"border-image: None;")
        self.labelTitle.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_5.addWidget(self.labelTitle)

        self.horizontalSpacer_3 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_5.addItem(self.horizontalSpacer_3)


        self.verticalLayout.addLayout(self.horizontalLayout_5)

        self.verticalSpacer_6 = QSpacerItem(20, 14, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer_6)

        self.verticalSpacer_2 = QSpacerItem(20, 14, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer_2)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setSpacing(10)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(50, -1, 50, -1)
        self.labelName = QLabel(self.frame)
        self.labelName.setObjectName(u"labelName")
        self.labelName.setStyleSheet(u"border-image: None;")

        self.horizontalLayout.addWidget(self.labelName)

        self.editName = QLineEdit(self.frame)
        self.editName.setObjectName(u"editName")

        self.horizontalLayout.addWidget(self.editName)


        self.verticalLayout.addLayout(self.horizontalLayout)

        self.verticalSpacer_4 = QSpacerItem(20, 15, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer_4)

        self.horizontalLayout_7 = QHBoxLayout()
        self.horizontalLayout_7.setObjectName(u"horizontalLayout_7")
        self.horizontalSpacer_4 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_7.addItem(self.horizontalSpacer_4)

        self.labelTheme = QLabel(self.frame)
        self.labelTheme.setObjectName(u"labelTheme")
        self.labelTheme.setStyleSheet(u"border-image: None;")
        self.labelTheme.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout_7.addWidget(self.labelTheme)

        self.buttonTheme = QPushButton(self.frame)
        self.buttonTheme.setObjectName(u"buttonTheme")

        self.horizontalLayout_7.addWidget(self.buttonTheme)

        self.horizontalSpacer_5 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_7.addItem(self.horizontalSpacer_5)


        self.verticalLayout.addLayout(self.horizontalLayout_7)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setSpacing(10)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(50, -1, 50, -1)
        self.buttonExport = QPushButton(self.frame)
        self.buttonExport.setObjectName(u"buttonExport")

        self.horizontalLayout_2.addWidget(self.buttonExport)

        self.buttonImport = QPushButton(self.frame)
        self.buttonImport.setObjectName(u"buttonImport")

        self.horizontalLayout_2.addWidget(self.buttonImport)


        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.horizontalLayout_4.setContentsMargins(100, -1, 100, -1)
        self.horizontalSpacer_6 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_4.addItem(self.horizontalSpacer_6)

        self.checkBorder = QCheckBox(self.frame)
        self.checkBorder.setObjectName(u"checkBorder")

        self.horizontalLayout_4.addWidget(self.checkBorder)

        self.horizontalSpacer_7 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_4.addItem(self.horizontalSpacer_7)


        self.verticalLayout.addLayout(self.horizontalLayout_4)

        self.verticalSpacer_3 = QSpacerItem(20, 14, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer_3)

        self.verticalSpacer = QSpacerItem(20, 14, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.buttonReset = QPushButton(self.frame)
        self.buttonReset.setObjectName(u"buttonReset")

        self.horizontalLayout_3.addWidget(self.buttonReset)

        self.buttonMore = QPushButton(self.frame)
        self.buttonMore.setObjectName(u"buttonMore")
        self.buttonMore.setFlat(False)

        self.horizontalLayout_3.addWidget(self.buttonMore)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer)

        self.buttonBack = QPushButton(self.frame)
        self.buttonBack.setObjectName(u"buttonBack")
        self.buttonBack.setFlat(False)

        self.horizontalLayout_3.addWidget(self.buttonBack)

        self.buttonSave = QPushButton(self.frame)
        self.buttonSave.setObjectName(u"buttonSave")
        self.buttonSave.setFlat(False)

        self.horizontalLayout_3.addWidget(self.buttonSave)


        self.verticalLayout.addLayout(self.horizontalLayout_3)


        self.verticalLayout_2.addWidget(self.frame)


        self.verticalLayout_3.addLayout(self.verticalLayout_2)


        self.retranslateUi(Dialog)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"MainWindow", None))
#if QT_CONFIG(tooltip)
        self.labelTitle.setToolTip("")
#endif // QT_CONFIG(tooltip)
        self.labelTitle.setText(QCoreApplication.translate("Dialog", u"<html><head/><body><p><span style=\" font-size:16pt; font-weight:600;\">Settings</span></p></body></html>", None))
#if QT_CONFIG(tooltip)
        self.labelName.setToolTip("")
#endif // QT_CONFIG(tooltip)
        self.labelName.setText(QCoreApplication.translate("Dialog", u"Name:", None))
#if QT_CONFIG(tooltip)
        self.editName.setToolTip("")
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(tooltip)
        self.labelTheme.setToolTip("")
#endif // QT_CONFIG(tooltip)
        self.labelTheme.setText(QCoreApplication.translate("Dialog", u"Theme:", None))
#if QT_CONFIG(tooltip)
        self.buttonTheme.setToolTip("")
#endif // QT_CONFIG(tooltip)
        self.buttonTheme.setText(QCoreApplication.translate("Dialog", u"Change", None))
#if QT_CONFIG(tooltip)
        self.buttonExport.setToolTip("")
#endif // QT_CONFIG(tooltip)
        self.buttonExport.setText(QCoreApplication.translate("Dialog", u"Export timetable", None))
#if QT_CONFIG(tooltip)
        self.buttonImport.setToolTip("")
#endif // QT_CONFIG(tooltip)
        self.buttonImport.setText(QCoreApplication.translate("Dialog", u"Import timetable", None))
#if QT_CONFIG(tooltip)
        self.checkBorder.setToolTip("")
#endif // QT_CONFIG(tooltip)
        self.checkBorder.setText(QCoreApplication.translate("Dialog", u"Window Border", None))
#if QT_CONFIG(tooltip)
        self.buttonReset.setToolTip("")
#endif // QT_CONFIG(tooltip)
        self.buttonReset.setText(QCoreApplication.translate("Dialog", u"Reset", None))
#if QT_CONFIG(tooltip)
        self.buttonMore.setToolTip("")
#endif // QT_CONFIG(tooltip)
        self.buttonMore.setText(QCoreApplication.translate("Dialog", u"More", None))
#if QT_CONFIG(tooltip)
        self.buttonBack.setToolTip("")
#endif // QT_CONFIG(tooltip)
        self.buttonBack.setText(QCoreApplication.translate("Dialog", u"Back", None))
#if QT_CONFIG(tooltip)
        self.buttonSave.setToolTip("")
#endif // QT_CONFIG(tooltip)
        self.buttonSave.setText(QCoreApplication.translate("Dialog", u"Save", None))
    # retranslateUi

