#!/bin/bash

if [[ $1 = '-a' ]]
then
ARCH=x86_64 appimagetool ~/Temporary/dist/timetable ~/Temporary/dist/timetable-lin.app
else
mkdir ~/Temporary/dist
mkdir ~/Temporary/dist/timetable
mkdir ~/Temporary/dist/timetable/lib
mkdir ~/Temporary/dist/timetable/assets
mkdir ~/Temporary/dist/timetable/assets/ui
python -m compileall ./timetable.py
python -m compileall ./assets/ui/timetable_ui.py
python -m compileall ./assets/ui/subject_ui.py
python -m compileall ./assets/ui/settings_ui.py
python -m compileall ./assets/ui/themes_ui.py
python -m compileall ./assets/ui/custom_ui.py
python -m compileall ./assets/ui/assignment_ui.py
python -m compileall ./assets/ui/assignmentsadd_ui.py
python -m compileall ./assets/ui/assignmentswindow_ui.py
python -m compileall ./assets/ui/images_rc.py
python -m compileall ./assets/ui/screenshots_rc.py
python -m compileall ./lib/config.py
python -m compileall ./lib/lessonpoints.py
python -m compileall ./lib/singleinstance.py
python -m compileall ./lib/themecolors.py
python -m compileall ./lib/termdates.py
cp -r ./__pycache__/timetable.cpython-310.pyc ~/Temporary/dist/timetable/timetable.pyc
cp -r ./assets/ui/__pycache__/timetable_ui.cpython-310.pyc ~/Temporary/dist/timetable/assets/ui/timetable_ui.pyc
cp -r ./assets/ui/__pycache__/subject_ui.cpython-310.pyc ~/Temporary/dist/timetable/assets/ui/subject_ui.pyc
cp -r ./assets/ui/__pycache__/settings_ui.cpython-310.pyc ~/Temporary/dist/timetable/assets/ui/settings_ui.pyc
cp -r ./assets/ui/__pycache__/themes_ui.cpython-310.pyc ~/Temporary/dist/timetable/assets/ui/themes_ui.pyc
cp -r ./assets/ui/__pycache__/custom_ui.cpython-310.pyc ~/Temporary/dist/timetable/assets/ui/custom_ui.pyc
cp -r ./assets/ui/__pycache__/assignment_ui.cpython-310.pyc ~/Temporary/dist/timetable/assets/ui/assignment_ui.pyc
cp -r ./assets/ui/__pycache__/assignmentsadd_ui.cpython-310.pyc ~/Temporary/dist/timetable/assets/ui/assignmentsadd_ui.pyc
cp -r ./assets/ui/__pycache__/assignmentswindow_ui.cpython-310.pyc ~/Temporary/dist/timetable/assets/ui/assignmentswindow_ui.pyc
cp -r ./assets/ui/__pycache__/map_ui.cpython-310.pyc ~/Temporary/dist/timetable/assets/ui/map_ui.pyc
cp -r ./assets/ui/__pycache__/images_rc.cpython-310.pyc ~/Temporary/dist/timetable/assets/ui/images_rc.pyc
cp -r ./assets/ui/__pycache__/backgrounds_rc.cpython-310.pyc ~/Temporary/dist/timetable/assets/ui/backgrounds_rc.pyc
cp -r ./assets/ui/__pycache__/screenshots_rc.cpython-310.pyc ~/Temporary/dist/timetable/assets/ui/screenshots_rc.pyc
cp -r ./assets/ui/button.qss ~/Temporary/dist/timetable/assets/ui/button.qss
cp -r ./assets/ui/icons ~/Temporary/dist/timetable/assets/ui/icons
cp -r ./lib/__pycache__/config.cpython-310.pyc ~/Temporary/dist/timetable/lib/config.pyc
cp -r ./lib/__pycache__/lessonpoints.cpython-310.pyc ~/Temporary/dist/timetable/lib/lessonpoints.pyc
cp -r ./lib/__pycache__/singleinstance.cpython-310.pyc ~/Temporary/dist/timetable/lib/singleinstance.pyc
cp -r ./lib/__pycache__/themecolors.cpython-310.pyc ~/Temporary/dist/timetable/lib/themecolors.pyc
cp -r ./lib/__pycache__/termdates.cpython-310.pyc ~/Temporary/dist/timetable/lib/termdates.pyc
cp -r ./lib/notifypy ~/Temporary/dist/timetable/lib/
cp -r ./lib/loguru ~/Temporary/dist/timetable/lib/

cp -r ./appimage/AppRun ~/Temporary/dist/timetable/AppRun
cp -r ./appimage/Timetable.desktop ~/Temporary/dist/timetable/Timetable.desktop
cp -r ./assets/ui/icon.png ~/Temporary/dist/timetable/
cp -r ./assets/ui/icon.png ~/Temporary/dist/timetable/assets/ui/
ARCH=x86_64 appimagetool ~/Temporary/dist/timetable ~/Temporary/dist/timetable-lin.app
fi
